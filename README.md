Name: Aulia Rosyida

NPM: 1706025346

Class: Advance Programming - B

Hobby: calligraphy drawing, travelling, coding

## Links

git exercise link: https://gitlab.com/aulia.rosyida/my-first-git-repo

Javari Park Festival link: https://gitlab.com/aulia.rosyida/ProgFound2-Assignments/tree/master/assignment-3

## My Notes

# Git Branches Usage

reason why we should use git branches: Karena gitLab digunakan para programmer dalam menyusun suatu projek program bersama-sama. Agar memudahkan pengerjaan projek tersebut dapat menggunakan branching. Branching juga berguna agar pekerjaan programmer yang sudah dikerjakan sebelum-sebelumnya dapat disimpan, sehingga ketika melakukan perubahan yang membuat kesalahan pada code nya dapat dilakukan revert pada branch lain, sehingga branch master dapat dikembalikan seperti semula.

describer how I use git branches on that repository: Branch dapat dikatakan sebagai pointer yang dapat bergerak ke salah satu commit yang kita inginkan. Default branch adalah master. Ketika saya sudah membuat commit di awal, branch master ditujukan ke commit terakhir yang ingin dibuat atau ditambahkan. Cara menggunakan branch:
1- Pertama-tama membuat branch baru terlebih dahulu, ketik "git branch <NAMA BRANCH>"
2- Memindahkan branch baru menggunakan "git checkout <NAMA BRANCH>"
poin 1-2 diatas dapat langsung dipanggil dengan "git checkout -b <NAMA BRANCH>"
3- Cek sedang berada di branch mana dengan "git branch -v"
4- Melakukan perubahan pada file yang diinginkan
5- setelah menyimpan file tsb dengan "git add" pada branch lain, melakukan merge dengan branch master ketik "git merge <BRANCH NAME>"

# Git Revert Usage

Find a scenario so I should use git revert : ketika terjadi problem saat melakukan "git merge" sehingga terjadi merge conflict condition. Contohnya ketika seorang programmer sedang memperbaiki method pada codenya (cth: Javari Park pada TP DDP 2) dan ternyata code yang dia tambahkan ketika di merge dan di push mengalami konflik dengan code sebelumnya, sehingga kita membutuhkan pengembalian ke commit sebelum terakhir.

describe how I use git revert on that repository: 
1- ketik "git log" 
2- pilih commit yang ingin dikembalikan dengan mengetik "git checkout <commit hash>". Maka, kita diberi akses untuk melihat ke state pada file yang memiliki commit tsb.
3- ketik "cat <NAMA FILE>"
4- setelah lihat file tsb. kembali ke HEAD dengan ketik "git checkout master" uuntuk kembali ke commit terakhir pada branch master dan ketik "git revert <comit hash>" untuk kembali ke commit yang diinginkan
5- apabila terjadi konflik, melakukan perbaikan pada file, kemudian ketik "git add ." dan "git revert --continue" klik esc, type ":wq" dan ketik enter.
6- cek "git log" untuk melihat last commit dengan word "revert"

